set t_Co=256
set nocompatible
filetype off

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

filetype plugin indent on
set background=dark
colorscheme xoria256
syntax on

set directory=~/.vim/tmp/
set backupdir=~/.vim/tmp/

if version >= 703
	set undofile
	set undodir=~/.vim/undo/
	set cc=80,120
    set relativenumber

    autocmd InsertEnter * :set number
    autocmd InsertLeave * :set relativenumber
else
    set number
endif

set cursorline
set hidden
set ruler
set hlsearch
set autoindent
set copyindent
set history=1000
set showmode
set backspace=indent,eol,start
set incsearch
set hlsearch
set lazyredraw

set scrolloff=10

set tabstop=4
set shiftwidth=4
set expandtab
set pastetoggle=<F4>

set list
set listchars=trail:⋅,tab:\ \ 

set cpoptions+=$
set virtualedit=all
set wildmenu

set laststatus=2

set statusline=%f                           " file name
set statusline+=\ %m                        " modified
set statusline+=\ %r                        " read only
set statusline+=\ %{fugitive#statusline()}  " fugitive status

set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L[%p%%]            " line X of Y [percent of file]
set statusline+=\ Col:%c                    " current column
set statusline+=\ Buf:%n                    " Buffer number
set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor

let NERDTreeShowBookmarks=1
let g:CommandTMatchWindowAtTop=1
let g:CommandTMaxHeight=30
let NERDTreeIgnore = ['\.pyc$']

let g:gundo_preview_height=20
let g:gundo_preview_bottom=1

"let g:proj_flags="imstvg"

"autocmd BufEnter * syntax on

"autocmd BufReadPost *
" 	\ if line("'\"") > 1 && line("'\"") <= line("$") |
" 	\ exe "normal! g`\"" |
" 	\ endif

autocmd FileType php,js,xml,ini,css,phtml,html autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))
autocmd WinLeave * set nocul
autocmd WinEnter * set cul

map <F3> :NERDTreeFind<CR>
nmap <C-W>! <Plug>Kwbd
noremap <F5> :GundoToggle<CR>
map <F2> :NERDTreeToggle<CR>
map <Leader>s :SessionList<CR>
map <Leader>k :Kwbd<CR>
imap [1;2R <Esc>:w<CR>i
nmap [1;2R :w<CR>

nmap <kPlus> <C-W>+
nmap <kMinus> <C-W>-
nmap <kDivide> <C-W><
nmap <kMultiply> <C-W>>
nmap <kPoint> <C-W>=
nmap <kEnter> <C-W>_<C-W><Bar>
set winminheight=0
set winminwidth=0

nmap <C-Up> <C-W>k
nmap <C-Down> <C-W>j
nmap <C-Left> <C-W>h
nmap <C-Right> <C-W>l

nmap <F1> <NOP>
map <F1> <Esc>
imap <F1> <Esc>

nmap <Leader>sci :!svn ci % -m "
nmap <Leader>sdi :!svn di %<CR>

nmap <Leader>t :CtrlP<CR>
nmap <Leader>z :bprev<CR>
nmap <Leader>x :bnext<CR>

" disable arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

set <k1>=Oq
set <k2>=Or
set <k3>=Os
set <k4>=Ot
set <k5>=Ou
set <k6>=Ov
set <k7>=Ow
set <k8>=Ox
set <k9>=Oy
set <k0>=Op

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
