#!/bin/bash
git submodule init
git submodule update

ln -s ~/Workspace/confiles/vim ~/.vim
ln -s ~/Workspace/confiles/vimrc ~/.vimrc

ln -s ~/Workspace/confiles/gitconfig ~/.gitconfig
ln -s ~/Workspace/confiles/git_template ~/.git_template
ln -s ~/Workspace/confiles/gitignore_global ~/.gitignore_global

ln -s ~/Workspace/confiles/tmux.conf ~/.tmux.conf

sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

mv ~/.zshrc ~/.zshrc.oh-my-zsh
ln -s ~/Workspace/confiles/zshrc.oh-my ~/.zshrc
