tabs -4

alias ls="ls -G"
alias la="ls -a"
alias ll="ls -l"
alias lla="ll -a"

alias svns="svn st"
alias svna="svn add"
alias svnu="svn up"
alias svnc="svn commit"
alias svnd="svn diff"
alias svnel="svn propedit svn:log http://id-hub/master --revprop -r"
alias :q="exit"
alias cd..="cd .."
alias cdpwd="cd `pwd`"

alias grep="grep --color=auto"

alias gitsdc="git stash && git svn dcommit && git stash apply"

export EDITOR=vim
export PATH="$HOME/bin:/usr/local/bin:/opt/local/bin:$PATH"
